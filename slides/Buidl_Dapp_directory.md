---
title: BUIDL an Ethereum dApp Part 2!
tags: Blockchain, PDX, Talk
description: Slides for the meetup Blockchain PDX, buidling a dapp on Ethereum! View the slides with "Slide Mode".
---

## Welcome!

###### Snacks & drinks are available, please help yourself!

![](https://i.imgur.com/2sL87ZV.png)

<small>Join our slack group!</small>

<small>

* Wifi: "Blockchain Meetup", Password: "-"
* Code of Conduct: https://gitlab.com/TrevorJTClarke/blockchain-pdx/blob/master/Code_Of_Conduct.md

</small>

<small>Thank you to our sponsors: [Amberdata.io](https://amberdata.io), [Consensys](https://www.buidlnetwork.net/), [10net](10net.net)</small>

---

## Tonight's Happenings

* dAppy Hour (How was it?)
* Show & Tell
* Who's Hiring
* Demo - A quick look at the Ethereum blockchain
* BUIDL
* Questions & Answers
* Circles - Help me define this dApp :)

---

## Demo - Amberdata.io

###### Amberdata provides:
<small>

1. A blockchain & market data API
2. Dashboards for blockchain
3. Fun things like a Slackbot
4. Try it free & show me what you build!

</small>

---


## Let's BUIDL a dApp - Part 2!

### What are building?

Let's take a quick look at the final product, then dive into how to build it. This will include a quick recap of last meetup's contract.

##### Contracts for this dApp:
<small>

  * Portfolio - Built last meetup, [see code -->](https://gitlab.com/TrevorJTClarke/profile_vue)
  * Directory - This meetup!
  * Token - ERC20 Next meetup
  * Voting
  * Content Manager

</small>

---

## What is Ethereum? (Recap)

<small>"Ethereum is a global, open-source platform for decentralized applications. On Ethereum, you can write code that controls digital value, runs exactly as programmed, and is accessible anywhere in the world." -[Ethereum.org](https://www.ethereum.org/)</small>

###### A few key features:
<small>

1. **YOU** can write the code that runs in a decentralized blockchain

2. **ANYONE** can access or execute the code you write
3. Blockchain is **global**, **immutable**, **censorship-resistant**

</small>

---

## What is a dApp? (Recap)

<small>
A dApp (Decentralized Application) is a set of code that is executed within a decentralized ecosystem, that is accessible by anyone using a compatible protocol connection. Many dApps make use of wallet or blockchain clients to enable users to connect to their dApp.
</small>

###### Key Requirements:
<small>

1. Code (EX: Solidity) lives inside the blockchain
2. Users connect via protocol (RPC or HTTP)
3. In many cases require cryptocurrency to pay for fees
4. Access can be served with a static website (EX: Vuejs app we're building!)

</small>

---

### Let's get to the CODE

![](https://i.imgur.com/FBINvhy.png)

---

## Step 0. Overview

#### We need:
1. Remix - https://remix.ethereum.org
2. Project Repo - https://github.com/TrevorJTClarke/Circles

<small>

We're going to build:
* Smart Contract - Using Solidity
* Remix - Smart Contract IDE
* Small Frontend using Vue

</small>

---

### Step 2. Smart Contract - Directory.sol

###### Let's dive into Remix IDE

<small>

We're going to create a smart contract that can:
1. Add our portfolio to be seen publically
2. Keep track of all participants in the directory

</small>

```javascript
contract Directory {
  uint256 public totalParticipants;
  address[] public participants;
  mapping(address => address) public participantContracts;

  event ParticipantAdded(address indexed _participant, address indexed _portfolio);
}
```

---

### Step . Start with demo project!

###### We are going to start from a few basic standard components:
1. Grab the project:
```bash
git clone git@github.com:TrevorJTClarke/Circles.git
```

2. Open and run project:
```bash
$ cd Circles
$ npm i
$ npm run serve
```

---

### Step . Vuejs Frontend Application

<small>

1. Setup a new component for showing the Directory!
2. Read data from blockchain with Amberdata.io

</small>

```html
<template>
  <div class="list">
    <div class="list-item" v-for="item in directory" :key="item">
      <h3>{{ item }}</h3>
      <router-link :to="{ name: 'portfolio', params: { hash: item }}">View Portfolio</router-link>
    </div>
  </div>
</template>

<script>
export default {
  name: 'Directory',

  ...
}
</script>
```

---

### Step X. Complete dApp!

![](https://i.imgur.com/lqcm8XT.png)

---

### Questions? Answers?

---

### Circles - Meetup BUIDL dApp

<small>
  Should we keep building this?
  Help me define what this becomes!
</small>

![architecture](https://i.imgur.com/yzAEqae.png)

---

# Resources

* [Meetup Archive Repository](https://gitlab.com/TrevorJTClarke/blockchain-pdx)
* [Gitlab Slides](https://hackmd.io/@trevor/buidldapp-part2#/)
* [Circles - dApp](https://github.com/TrevorJTClarke/Circles)
* [Remix IDE](https://remix.ethereum.org)
* [Amberdata API](https://docs.amberdata.io/)
