---
title: BUIDL an Ethereum dApp
tags: Blockchain, PDX, Talk
description: Slides for the meetup Blockchain PDX, buidling a dapp on Ethereum! View the slides with "Slide Mode".
---

## Welcome!

###### Snacks & drinks are available, please help yourself!

![](https://gitlab.com/TrevorJTClarke/blockchain-pdx/blob/master/images/eth_pnw.svg)
<small>Join our slack group!</small>

<small>

* Wifi: "10netPDXguest", Password: "-"
* Code of Conduct: https://gitlab.com/TrevorJTClarke/blockchain-pdx/blob/master/Code_Of_Conduct.md

</small>

<small>Thank you to our sponsors: [Amberdata.io](https://amberdata.io), [Consensys](https://www.buidlnetwork.net/), [10net](10net.net)</small>

---

## Let's BUIDL a dApp!

### Wait, what is a dApp?

![](https://media.giphy.com/media/fMvvwdTWamlA4/giphy.gif)

I'm glad you asked, but first a quick overview...

---

# What is Blockchain?

<small>Definition: An immutable decentralized ledger, allowing many parties to transact value or data within a trustless peer to peer connection. The ledger is made up of blocks, which contain a timestamp, a hash of the data or transaction, and a reference to the previous block. A new block is only accepted if it meets the consensus mechanism defined by the protocol, usually calculated by a miner or validator.

"Blockchain was invented by a person (or group of people) using the name Satoshi Nakamoto in 2008 to serve as the public transaction ledger of the cryptocurrency bitcoin." -[Wikipedia](https://en.wikipedia.org/wiki/Blockchain)
</small>

---

## What is Ethereum?

<small>"Ethereum is a global, open-source platform for decentralized applications. On Ethereum, you can write code that controls digital value, runs exactly as programmed, and is accessible anywhere in the world." -[Ethereum.org](https://www.ethereum.org/)</small>

###### A few key features:
<small>

1. **YOU** can write the code that runs in a decentralized blockchain

2. **ANYONE** can access or execute the code you write
3. Blockchain is **global**, **immutable**, **censorship-resistant**

</small>

---

## What is a dApp?

<small>
A dApp (Decentralized Application) is a set of code that is executed within a decentralized ecosystem, that is accessible by anyone using a compatible protocol connection. Many dApps make use of wallet or blockchain clients to enable users to connect to their dApp.
</small>

###### Key Requirements:
<small>

1. Code (EX: Solidity) lives inside the blockchain
2. Users connect via protocol (RPC or HTTP)
3. In many cases require cryptocurrency to pay for fees
4. Access can be served with a static website (EX: Vuejs app we're building!)

</small>

---

### Let's get to the CODE

![](https://gitlab.com/TrevorJTClarke/blockchain-pdx/blob/master/images/dApp_Architecture.png)

<small>

We're going to build:
* Smart Contract - Using Solidity
* Unit Tests
* Small Frontend using Embark & Vue

</small>

<small>Are there other frameworks?</small>

---

## Step 0. Install the stuff

#### We need:
1. nodejs & npm _(8.11.3 LTS or higher)_
2. Embark `npm -g install embark`

There are other _optional_ dependencies:
* Geth - An Ethereum client
* IPFS - A decentralized storage client

---

### Step 1. Start with demo project!

###### We are going to start from a few basic standard components:
1. Start new project:
```bash
embark new YOUR_dApp_NAME --template https://github.com/TrevorJTClarke/embark-vue-template
```

2. Open and run project:
```bash
$ cd YOUR_dApp_NAME
$ embark run
```

---

### Meta. Why Embark?

###### A few key things to notice right off the bat:

<small>

* Embark cli - bootstrapping all the things so you dont have to!
* Embark GUI - Good for status of all the services running
* Embark Cockpit - Smart Contract dev environment, everything you need in one!
* Vuejs + Webpack - Built in frontend, so you get off the ground fast
* [Amberdata API](https://amberdata.io/pricing) - Fast blockchain & market data

</small>

---

### Step 2. Smart Contract - Portfolio.sol

###### Let's dive into the IDE and start Solidity code

<small>

We're going to create a smart contract that can:
1. Contain our first & last name
2. Keep track of our skills
3. Help people know if we want to be hired

</small>

```solidity
contract Portfolio {
  bool public hireMe = false;
  uint public skillsTotal = 0;
  address public owner;
  string public firstName;
  string public lastName;

  ...
}
```

---

### Step X. Embark Cockpit

<small>Using the local cockpit, I can test the contract and make changes.</small>

<small>

Key features:
* Dashboard - See current local development status & output
* Deployment - Keep track of contracts & deployment networks
* Explorer - View all blockchain data in a simplified UI
* Editor - IDE setup for smart contract development

</small>

---

### Step X. Smart Contract Unit Tests

<small>We need to ensure this contract meets requirements!</small>

<small>Let's setup unit tests to check the work.</small>

```javascript
it("should set constructor owner", async function () {
  let result = await Portfolio.methods.owner().call();
  assert.strictEqual(result, accounts[0]);
});

it("set firstName value", async function () {
  await Portfolio.methods.updateFirstname('first').send();
  let result = await Portfolio.methods.firstName().call();
  assert.strictEqual(result, 'first');
});
```

---

### Step X. Vuejs Frontend Application

<small>GOAL: Establish a connection to our local blockchain using Embarkjs. The frontend will listen for a blockchain connection, bootstrap our ABI, then allow us to call the functions as defined by the ABI.</small>

<small>

1. Setup a new component for showing our Profile!
2. Setup a management form to submit data to blockchain

</small>

```html
<template>
  <div class="card">
    <h1>Hello</h1>
    <h4>My name is</h4>
  </div>
</template>

<script>
import EmbarkJS from 'Embark/EmbarkJS'

export default {
  name: 'HelloCard',

  ...
}
</script>
```

---

### Step X. Complete dApp!

![](https://gitlab.com/TrevorJTClarke/blockchain-pdx/blob/master/images/dApp_ui.png)

---

### Questions?

---

### Meetup Survey:

<small>Help me help you! If you can take 2 minutes to complete this Survey, it will help planning going forward!</small>

![](https://gitlab.com/TrevorJTClarke/blockchain-pdx/blob/master/images/blockchain_pdx_survey.svg)

Now go #BUIDL!

---

# Resources

* [Meetup Archive Repository](https://gitlab.com/TrevorJTClarke/blockchain-pdx)
* [Gitlab Slides]()
* [Embark Framework](https://embark.status.im/)
* [Unit testing refs](https://embark.status.im/docs/contracts_testing.html)
* [Amberdata API](https://docs.amberdata.io/)
